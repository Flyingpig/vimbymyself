"自动插入标头

autocmd BufNewFile *.py,*.sh exec ":call SetTitle()"

func SetTitle()
    if expand("%:e") == 'sh'  
        call append(0, "#!/bin/bash")
    elseif expand("%:e") == 'py'  
        call append(0, "#!/bin/python")
    endif  

    call append(1,"#******************************************")
    call append(2,"#File Name:       ".expand("%"))
    call append(3,"#Author:          guzhenzong")
    call append(4,"#Create_Time:     ".strftime("%Y-%m-%d" ))
    call append(5,"#Version:         v1.o                 ")
    call append(6,"#Mail:            goodluch.gu@outlook.com    ")
    call append(7,"#Description:     doyouwanttodo         ")
    call append(8,"#******************************************")
endfunc
